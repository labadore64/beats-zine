# What is this?

This is an open public repository to participate in the production of a currently-unnamed small-scale publication that specifically focuses on radical disabled experience, but also will include subjects from related struggles, technology, medicine and privacy politics, among other things.

Currently this issue doesn't have a theme but suggestions are always welcome.

This repository is designed to allow for active contributions on the project in multiple branches.

The final product is going to both be published on my website, as well as available online and designed to be printed and distributed with ordinary 8.5"x11" paper. 

# How can I participate?

Ask the project owner (labadore64 or forthegy) to add you to the list of contributors and you can be added to the project.

We are looking for the following:

- Theorists (especially focused on disability and economics, existentialism, religion, race, gender and art)
- Artists, poets
- Small editorials and opinions
- List of interesting sites

# Where do I put my contributions?

Make a directory under ``content/`` that is named after you, and put your contributions in there. Also, you should make your own custom branch and not push to master - we will merge your contributions later. 

# How are the contributions used? What are my rights?

We will assemble the zine into a single PDF that can be printed out and is designed to print both in color or black-and-white for small scale distribution. This zine will be published on my website as well. If your contriubtions don't get used, we might use it in another issue later. We may use the content to promote the zine, but we will never use your content without permission outside of this zine.

You retain all rights to your content. We just ask that we can republish it with the zine and allow small scale publication like we encourage. We want to spread the content like spores in the wind - so keep this in mind with what you want to contribute. Of course, you can publish this content anywhere else you desire.
